package utils;

public class HashUtil {
	
	public static char[] int2charArray( int[] a ) {
		char[] c = new char[a.length];
		for(int i=0 ; i < a.length ; ++i)
			c[i] = (char) a[i];
		return c;
	}

	public static String byteStr2hexStr(StringBuilder byteSB) {
		StringBuilder hexSB = new StringBuilder();
		for (int j = 0; j < byteSB.length(); ++j) {
			char c = byteSB.charAt(j);
			hexSB.append(Integer.toHexString(c / 16));
			hexSB.append(Integer.toHexString(c % 16));
		}
		return hexSB.toString();
	}
}
