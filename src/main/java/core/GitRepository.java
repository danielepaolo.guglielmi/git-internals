package core;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class GitRepository {
	
	private String repoPath;
	private Scanner s;

	public GitRepository(String repoPath) {
		this.repoPath = repoPath;
	}

	public String getHeadRef() throws FileNotFoundException {
		s = new Scanner(new File(repoPath + "/HEAD"));
		
		s.skip("ref:");
		String ret = s.next();
		
		return ret;
		
	}

	public String getRefHash(String path) throws FileNotFoundException {
		s = new Scanner(new File(repoPath + "/" + path));
		
		String ret = s.next();
		
		return ret;
	}

}