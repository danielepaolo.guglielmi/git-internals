package core;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.zip.InflaterInputStream;

public class GitObject {
	
	private static final String OBJECTS_DIR = "objects";
	private String repoPath;
	private String objHash;
	
	private InflaterInputStream in;
	private Scanner s;

	public GitObject(String repoPath, String objHash) {
		this.repoPath = repoPath;
		this.objHash = objHash;
	}
	
	public String getRepoPath() throws IOException {
		return this.repoPath;
	}
	
	public String getHash() throws IOException {
		return this.objHash;
	}
	
	public String getType() throws IOException {
		StringBuilder sbuild = getFullContent();
		sbuild.delete(sbuild.indexOf(" "), sbuild.length());
		return sbuild.toString();
	}
	
	protected int[] getBinaryContent() throws IOException {
		in = new InflaterInputStream(new FileInputStream(getPath()));
		int[] buf = new int[1024];
		
		int buf_i;
		for( buf_i = 0 ; in.available() > 0 ; buf_i++ )
			buf[buf_i] = in.read();
		
		return Arrays.copyOfRange(buf,0,buf_i);
	}
	
	protected StringBuilder getFullContent() throws IOException {
		s = new Scanner( new InflaterInputStream(new FileInputStream(getPath())));
		StringBuilder sbuild = new StringBuilder();
		
		while( s.hasNext() ) {
			sbuild.append(s.nextLine());
			sbuild.append("\n");
		}
		
		return sbuild;
	}
	
	protected String getPath() {
		return repoPath + "/" + OBJECTS_DIR + "/" + objHash.substring(0, 2) + "/" + objHash.substring(2);
	}

}
