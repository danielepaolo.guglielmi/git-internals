package core;

import java.io.IOException;

public class GitCommitObject extends GitObject {
	
	private static final String TREE = "tree";
	private static final String PARENT = "parent";
	private static final String AUTHOR = "author";

	public GitCommitObject(String repoPath, String objPath) {
		super(repoPath, objPath);
	}
	
	public String getTreeHash() throws IOException {
		StringBuilder sbuild = getFullContent();
		sbuild.delete(0, sbuild.indexOf("\0")+1);
		sbuild.delete(sbuild.indexOf(TREE), TREE.length()+1);
		sbuild.delete(sbuild.indexOf("\n"), sbuild.length());
		return sbuild.toString();
	}

	public String getParentHash() throws IOException {
		StringBuilder sbuild = getFullContent();
		sbuild.delete(0, sbuild.indexOf("\0")+1);
		sbuild.delete(sbuild.indexOf(TREE), sbuild.indexOf("\n")+1);
		sbuild.delete(sbuild.indexOf(PARENT), PARENT.length()+1);
		sbuild.delete(sbuild.indexOf("\n"), sbuild.length());
		return sbuild.toString();
	}

	public String getAuthor() throws IOException {
		StringBuilder sbuild = getFullContent();
		sbuild.delete(0, sbuild.indexOf("\0")+1);
		sbuild.delete(sbuild.indexOf(TREE), sbuild.indexOf("\n")+1);
		sbuild.delete(sbuild.indexOf(PARENT), sbuild.indexOf("\n")+1);
		sbuild.delete(sbuild.indexOf(AUTHOR), AUTHOR.length()+1);
		sbuild.delete(sbuild.indexOf(">")+1, sbuild.length());
		return sbuild.toString();
	}

}
