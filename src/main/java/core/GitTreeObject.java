package core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import utils.HashUtil;

public class GitTreeObject extends GitObject {

	public GitTreeObject(String repoPath, String objHash) {
		super(repoPath, objHash);
	}

	public List<String> getEntryPaths() throws IOException {
		ArrayList<String> ret = new ArrayList<String>(); 
		
		StringBuilder sbuild = getFullContent();
		sbuild.delete(0, sbuild.indexOf(" ")+1);
		sbuild.delete(0, sbuild.indexOf("\0")+1);
		while(sbuild.length() > 0) {
			int i = sbuild.indexOf(" ");
			if (i != -1) {
				sbuild.delete(0, sbuild.indexOf(" ")+1);
				ret.add(sbuild.substring(0, sbuild.indexOf("\0")));
				sbuild.delete(0, sbuild.indexOf("\0")+1);
			} else {
				sbuild.delete(0, sbuild.length());
			}
		}
		
		System.out.println(sbuild.toString());
		
		return ret;
	}

	public GitObject getEntry(String key) throws IOException {
		key += "\0";
        int hashCharLen = 20;
        
        StringBuilder sbuild = new StringBuilder(new String(HashUtil.int2charArray(this.getBinaryContent())));
        sbuild.delete(0, sbuild.indexOf(key)+key.length());
        sbuild.delete(sbuild.indexOf(key)+hashCharLen+1, sbuild.length());
        
        String objHash = HashUtil.byteStr2hexStr(sbuild);
        
        GitObject gitOb = new GitBlobObject(this.getRepoPath(), objHash);
        if (gitOb.getType().equals("tree"))
                gitOb = new GitTreeObject(this.getRepoPath(), objHash);
        return gitOb;

	}

}
