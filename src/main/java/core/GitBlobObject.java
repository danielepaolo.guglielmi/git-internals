package core;

import java.io.IOException;

public class GitBlobObject extends GitObject {

	public GitBlobObject(String repoPath, String objHash) {
		super(repoPath, objHash);
	}

	public String getContent() throws IOException {
		StringBuilder sbuild = getFullContent();
		sbuild.delete(0, sbuild.indexOf("\0")+1);
		return sbuild.toString();
	}

}
